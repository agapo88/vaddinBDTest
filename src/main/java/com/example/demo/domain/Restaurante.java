package com.example.demo.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by USUARIO on 20/07/2017.
 */
@Entity
public class Restaurante {
    private String nombre;
    private String direccion;
    private Boolean aceptaNinos;
    private int cantTrabajadores = 0;
    private int cantComensales = 0;
    private int estrellas;
    //private List< Restaurante > listaRestaurantes;

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Restaurante() {
        //listaRestaurantes = new ArrayList<>();
    }
/*
    public void adicionarRestaurante( Restaurante res ){
        listaRestaurantes.add( res );
    }

    public List<Restaurante> getListadoRestaurantes(){
        return listaRestaurantes;
    }
*/
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Boolean getAceptaNinos() {
        return aceptaNinos;
    }

    public void setAceptaNinos(Boolean aceptaNinos) {
        this.aceptaNinos = aceptaNinos;
    }

    public int getCantTrabajadores() {
        return cantTrabajadores;
    }

    public void setCantTrabajadores(int cantTrabajadores) {
        this.cantTrabajadores = cantTrabajadores;
    }

    public int getCantComensales() {
        return cantComensales;
    }

    public void setCantComensales(int cantComensales) {
        this.cantComensales = cantComensales;
    }

    public int getEstrellas() {
        return estrellas;
    }

    public void setEstrellas(int estrellas) {
        this.estrellas = estrellas;
    }
}

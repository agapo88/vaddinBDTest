package com.example.demo.visual;

import com.example.demo.domain.Restaurante;
import com.example.demo.domain.RestauranteRepository;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by USUARIO on 15/07/2017.
 */
@SpringUI
public class App extends UI {

    @Autowired
    RestauranteRepository restauranteRepository;
    //EstudianteRepository estudianteRepository;

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        VerticalLayout layout = new VerticalLayout();
        HorizontalLayout hlayout = new HorizontalLayout();

        Label titulo = new Label("REGISTRAR RESTAURANTE");
        TextField nombre = new TextField("Nombre:");
        TextField direccion = new TextField("Dirección:");
        CheckBox aceptaNinos = new CheckBox("Aceptan Niños:");
        TextField cantComensales = new TextField("Cant. Comensales:");
        TextField cantTrabajadores = new TextField("Cant. Trabajadores:");
        Button add = new Button( "Adicionar" );

        List<String> data = IntStream.range(1, 5).mapToObj(i -> "" + i).collect(Collectors.toList());
        NativeSelect estrellas = new NativeSelect<>("Seleccione estrellas", data);

        estrellas.setEmptySelectionAllowed(false);
        estrellas.setSelectedItem( data.get(0) );

        cantComensales.setValue( "0" );
        cantTrabajadores.setValue( "0" );

        Grid<Restaurante> grid = new Grid<>();
        grid.setWidth( "100%" );
        grid.addColumn( Restaurante::getId ).setCaption("Id");
        grid.addColumn( Restaurante::getNombre ).setCaption("Nombre");
        grid.addColumn( Restaurante::getDireccion ).setCaption("Direccion");
        grid.addColumn( Restaurante::getAceptaNinos).setCaption("Aceptan Ninos");
        grid.addColumn( Restaurante::getCantComensales).setCaption("Cant. Comensales");
        grid.addColumn( Restaurante::getCantTrabajadores).setCaption("Cant. Trabajadores");
        grid.addColumn( Restaurante::getEstrellas).setCaption("Estrellas");

        add.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {

                boolean error = false;

                // VALIDACIONES
                if( nombre.getValue().length() < 3 ){
                    error = true;
                    Notification.show("Nombre no valido, verifique.");
                }
                else if( direccion.getValue().length() <= 10 ){
                    error = true;
                    Notification.show("La direccion debe tener mas de 10 caracteres");
                }
                else if( Integer.parseInt( cantComensales.getValue() ) <= 0 ){
                    error = true;
                    Notification.show("La cantidad de Comensales debe ser mayor a 0");
                }
                else if( Integer.parseInt( cantTrabajadores.getValue() ) <= 0 ){
                    error = true;
                    Notification.show("La cantidad de Trabajadores debe ser mayor a 0");
                }

                if( !error ){
                    Restaurante e = new Restaurante();

                    e.setNombre( nombre.getValue() );
                    e.setDireccion( direccion.getValue() );
                    e.setAceptaNinos( aceptaNinos.getValue() );
                    e.setCantComensales(Integer.parseInt(cantComensales.getValue()));
                    e.setCantTrabajadores(Integer.parseInt(cantTrabajadores.getValue()));
                    //e.setEstrellas(  );

                    restauranteRepository.save( e );
                    grid.setItems( restauranteRepository.findAll() );

                    // RESETEAR VALORES
                    nombre.clear();
                    direccion.clear();
                    aceptaNinos.clear();
                    cantComensales.setValue( "0" );
                    cantTrabajadores.setValue( "0" );

                    Notification.show("Restaurante agregado.");

                    estrellas.setEmptySelectionAllowed(false);
                    estrellas.setSelectedItem( data.get(0) );

                }
            }
        });

        layout.addComponents( titulo  );
        hlayout.addComponents( nombre, direccion, aceptaNinos, cantComensales, cantTrabajadores, estrellas, add );
        hlayout.setComponentAlignment( aceptaNinos, Alignment.MIDDLE_CENTER );
        hlayout.setComponentAlignment( add, Alignment.BOTTOM_RIGHT );

        layout.addComponents( hlayout, grid );
        //layout.addComponent( new Button("Hola"));
        setContent( layout );

    }
}
